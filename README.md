
# BDD Test Framework v.01

Test Framework to encourage the adoption of BDD and automated acceptance testing. Clone this repo into your existing project to be able to include acceptance tests for your source code.

Run `npm i` to install packages

Update `behavepro.config.json` with your JIRA project details (admin permissions required)

Run `npm run cucumber` to run tests

Following outlines BDD and how to start automating acceptance tests.

# Summary
## What Is BDD? 

At the core of Behaviour Driven Development, is understanding - the intention is to provide everyone (BA / PO, QA, Dev) an understanding of the system under test. It is not a testing methodology, rather a development approach. It builds upon the core practices of TDD - that is create failing tests BEFORE any code, build the software to pass the failing tests, refactor and repeat - however when employing BDD, rather than focus on the low level details of the system it focuses on the high level aka the behaviour. 

Done right, BDD will PREVENT bugs rather than force you to REACT to them, by ironing out common miscommunications between teams at the start. The benefits are:

- Less uneccessary refactoring
- Accessible interface to the system under test
- A direct map between the system and requirements
- A more cohesive development approach
- A better system overall, which saves time and costs
## BDD For Dummies

The Three-Pronged Approach is generally accepted as the best way to implement BDD in your software:

1) Collaboration
    - Discussions with BA/PO, QA, Dev BEFORE any software development about the feature to be developed. Missing out a key role in these discussions i.e. BA, QA increases the risk of miscommunication.

2) Documenation
    - Scenarios are captured and documented (in this case we use Jira with the BehavePro plugin tool, meaning that Jira becomes the single source of truth for all requirements. Avoids cowboy requirements and YOLO merges as everything must come via collaboration first)

3) Automation
    - Following TDD practices, export the scenarios to your test framework and develop the glue and source code in parallel to satisfy the acceptance tests until they pass. Refactor and repeat.
# What are BDD acceptance tests? 

Automated tests which evaluate the behaviour of the system and validate what is being developed against the original intended functionality as specified by the business acceptance criteria, in the form of scenarios. High level syntax.

Follows the same practice as TDD - failing tests written first, source code written to pass the tests, refactor and repeat.

In this framework the acceptance tests are presented as feature files, written in a plain english Gherkin syntax to ensure an easy to understand interface.
## Acceptance Tests vs Unit Tests

BDD can in fact be used at all levels of testing - it all depends on where the business has a vested interest. However more frequently, we find that low level forms of testing are generally of interest to tech teams whilst high level forms of testing are generally of interest to business teams - thus BDD is much more common in high level end to end acceptance tests than low level unit tests. The below outlines some reasons for why this might be:

- Unit tests focus on IMPLEMENTATION; acceptance tests focus on BEHAVIOUR
- Unit tests focus on METHODS; acceptance tests focus on working COMPONENTS
- Unit tests utlise MOCKS; acceptance tests focus on END TO END journeys
- Unit tests key parties of interest are primarily DEVELOPERS; acceptance tests are primarily for BUSINESS

Remember - Unit tests are the isolated picture whereas acceptance tests look at the bigger picture. They intentionally have a high level interface as scenarios / Gherkin plain english syntax so that ANYONE can read and understand them. KISS - Keep It Simple!
# Tools

Jira

BehavePro

Cucumber.js / Gherkin

Typescript
# Usage

This section will outline BDD in the context of this framework from start to finish.
# How To Automate

1) Run $ npm run features

2) Locate the feature you will be automating in features/behaveProFeatures and open the feature file. Move it under the parent /features folder.

3) In the /stepDefinitions folder, create a new step definitions file

4) Run $ npm run cucumber to run the tests. The first run provides the boilerplate.

5) Copy the boilerplate into the step definitions file

6) You are now ready to automate the acceptance scenarios :) 

Ensure that every scenario has the JIRA id as a tag in all caps e.g. @TV-100. This allows all your scenarios to be run only, when specified by the tag option.
Also add specific scenario tags for debugging purposes, in lowercase e.g. @scn1, @scn2 etc. Cucumber allows you to run sub tags in conjuction with main tags i.e. --tags='@TV-100 and @scn1' will run the first scenario of the feature TV-100 only.

See Cucumber docs for more info: https://cucumber.io/docs/cucumber/api/#tags

NOTES:
Please note this framework is a work in progress and as such this document is likely to change. Please feedback to Hasan Gani (mohammed.gani@dunelm.com) for comments and improvements.