require("dotenv").config();

import { After, Before, Given, Then, When } from "cucumber";

import { RequestData } from "../support/Example";
import chai from "chai";
import chaiHttp from "chai-http";

chai.use(chaiHttp);
const expect = chai.expect;
const And = Given;
const base = process.env.BASE_DOMAIN;

const body: RequestData = {
  id: 100
};

Given("I know the endpoint {string}", endpoint => {
  this.endpoint = endpoint;
  return true;
});

When("I make the request {string}", async method => {
  method === "GET"
    ? (this.res = await chai.request(base).get(this.endpoint))
    : (this.res = await chai
        .request(base)
        .post(this.endpoint)
        .send(body));
});

Then("I should see a response status {int}", async status => {
  expect(this.res.status).to.equal(status);
});
