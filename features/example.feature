Feature: Example Feature

  As an example
  I want to create BDD tests
  So that I can create quality software

  @scn1
  Scenario Outline: Request to endpoint returns 200
    Given I know the endpoint <endpoint>
    When I make the request <method>
    Then I should see a response status <status>

    Examples:
      | endpoint | method | status |
      | "/200"   | "GET"  | 200    |
      | "/200"   | "POST" | 200    |

